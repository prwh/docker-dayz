# Docker Day 2016

## Getting Started

1. Have [docker for windows](https://docs.docker.com/docker-for-windows/) installed and running, you may need to ask IT for assistance as you may need to enable virtualisation.
2. Clone this repo.
3. Make sure you have drive sharing enabled with the drive you pulled this repo down to (right click the docker logo in the system tray and click settings):
![like so](http://i.imgur.com/4k6Yecr.png)
4. Open up a cmd or powershell window in the repository root and execute ```docker-compose up -d``` (or ```docker-compose up``` if you want to see the output).

Docker should now spin up the hack day base environment:

## The environment

The environment consists of several docker containers:

* A MongoDb on mongodb://localhost:3001
* A RabbitMq running on http://localhost:3002
* The Rabbit managment portal on http://localhost:3003
* A rest api named api running on http://localhost:3004
* A website named display which displays output from the api running on http://localhost:3005
* A Console app listening to the hackday rabbit queue

Note all the above urls are for use from outside of the docker environment (e.g. a web browser or mongochef running on your machine) within the docker environment you will have to connect to [container-alias]:[port] for an example see api/server.js where it connects to mongo. 

![System Diagram](http://i.imgur.com/0dvsGbr.png)

## The Challenges

We have come up with two challenges:

1. BE CREATIVE: Build one or more containers that interact with the realtime display
2. BE DESTRUCTIVE: Build one or more docker containers that test the base environment or any of the other teams' containers.

**Hint**: you may want to figure out how to disable the EventPublisher first, otherwise your output will be littered with: ![Message](http://i.imgur.com/WjyZjJG.png)



